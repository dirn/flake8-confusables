=================================
flake8-confusables |build status|
=================================

.. |build status| image:: https://gitlab.com/dirn/flake8-confusables/badges/master/pipeline.svg
   :target: https://gitlab.com/dirn/flake8-confusables/commits/master

A module that adds a plugin for checking for ambiguous identifiers (using
confusables_) to flake8_.

Install the extension::

    $ pip3 install flake8-confusables

and run flake8.

.. _confusables: https://pypi.org/project/confusables/
.. _flake8: https://gitlab.com/pycqa/flake8
